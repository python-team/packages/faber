# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/faber_bench/main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_main(object):
    def setupUi(self, main):
        main.setObjectName("main")
        main.resize(800, 447)
        main.setMinimumSize(QtCore.QSize(200, 100))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/logo_small.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        main.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(main)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QtCore.QSize(0, 80))
        self.widget.setMaximumSize(QtCore.QSize(16777215, 80))
        self.widget.setStyleSheet("border-image: url(:/images/bg.png) 0 0 0 0 stretch stretch;")
        self.widget.setObjectName("widget")
        self.gridLayout = QtWidgets.QGridLayout(self.widget)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.gridLayout.setContentsMargins(-1, -1, 1, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.sourcedir = QtWidgets.QLabel(self.widget)
        self.sourcedir.setMinimumSize(QtCore.QSize(0, 40))
        self.sourcedir.setMaximumSize(QtCore.QSize(16777215, 40))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(128, 118, 103))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.sourcedir.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setItalic(True)
        self.sourcedir.setFont(font)
        self.sourcedir.setObjectName("sourcedir")
        self.gridLayout.addWidget(self.sourcedir, 0, 3, 1, 1)
        self.sourcedir_label = QtWidgets.QLabel(self.widget)
        self.sourcedir_label.setMinimumSize(QtCore.QSize(0, 40))
        self.sourcedir_label.setMaximumSize(QtCore.QSize(16777215, 40))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(128, 118, 103))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.sourcedir_label.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.sourcedir_label.setFont(font)
        self.sourcedir_label.setObjectName("sourcedir_label")
        self.gridLayout.addWidget(self.sourcedir_label, 0, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 1, 1, 1)
        self.builddir_label = QtWidgets.QLabel(self.widget)
        self.builddir_label.setMinimumSize(QtCore.QSize(0, 40))
        self.builddir_label.setMaximumSize(QtCore.QSize(16777215, 40))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(128, 118, 103))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.builddir_label.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.builddir_label.setFont(font)
        self.builddir_label.setObjectName("builddir_label")
        self.gridLayout.addWidget(self.builddir_label, 1, 2, 1, 1)
        self.logo = QtWidgets.QLabel(self.widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.logo.sizePolicy().hasHeightForWidth())
        self.logo.setSizePolicy(sizePolicy)
        self.logo.setMinimumSize(QtCore.QSize(0, 100))
        self.logo.setMaximumSize(QtCore.QSize(200, 100))
        self.logo.setBaseSize(QtCore.QSize(0, 0))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(128, 118, 103))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.logo.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.logo.setFont(font)
        self.logo.setAutoFillBackground(False)
        self.logo.setStyleSheet("")
        self.logo.setTextFormat(QtCore.Qt.RichText)
        self.logo.setScaledContents(True)
        self.logo.setObjectName("logo")
        self.gridLayout.addWidget(self.logo, 0, 0, 1, 1)
        self.builddir = QtWidgets.QLabel(self.widget)
        self.builddir.setMinimumSize(QtCore.QSize(0, 40))
        self.builddir.setMaximumSize(QtCore.QSize(16777215, 40))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(128, 118, 103))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        self.builddir.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setItalic(True)
        self.builddir.setFont(font)
        self.builddir.setObjectName("builddir")
        self.gridLayout.addWidget(self.builddir, 1, 3, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 0, 4, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem3, 1, 4, 1, 1)
        self.verticalLayout.addWidget(self.widget)
        self.splitter = QtWidgets.QSplitter(self.centralwidget)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName("splitter")
        self.tree = QtWidgets.QTreeView(self.splitter)
        self.tree.setBaseSize(QtCore.QSize(0, 250))
        self.tree.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tree.setEditTriggers(QtWidgets.QAbstractItemView.DoubleClicked)
        self.tree.setRootIsDecorated(True)
        self.tree.setObjectName("tree")
        self.tree.header().setDefaultSectionSize(200)
        self.output = QtWidgets.QTextEdit(self.splitter)
        self.output.setBaseSize(QtCore.QSize(0, 250))
        self.output.setStyleSheet("")
        self.output.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.output.setLineWrapMode(QtWidgets.QTextEdit.NoWrap)
        self.output.setReadOnly(True)
        self.output.setObjectName("output")
        self.verticalLayout.addWidget(self.splitter)
        self.progress = ProgressBar(self.centralwidget)
        self.progress.setProperty("value", 0)
        self.progress.setObjectName("progress")
        self.verticalLayout.addWidget(self.progress)
        main.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(main)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 30))
        self.menubar.setObjectName("menubar")
        self.project = QtWidgets.QMenu(self.menubar)
        self.project.setObjectName("project")
        self.settings = QtWidgets.QMenu(self.menubar)
        self.settings.setObjectName("settings")
        self.help = QtWidgets.QMenu(self.menubar)
        self.help.setObjectName("help")
        self.menuView = QtWidgets.QMenu(self.menubar)
        self.menuView.setObjectName("menuView")
        main.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(main)
        self.statusbar.setObjectName("statusbar")
        main.setStatusBar(self.statusbar)
        self.toolbar = QtWidgets.QToolBar(main)
        self.toolbar.setObjectName("toolbar")
        main.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)
        self.quit = QtWidgets.QAction(main)
        self.quit.setObjectName("quit")
        self.scheduler = QtWidgets.QAction(main)
        self.scheduler.setObjectName("scheduler")
        self.about = QtWidgets.QAction(main)
        self.about.setObjectName("about")
        self.view_public_artefacts = QtWidgets.QAction(main)
        self.view_public_artefacts.setObjectName("view_public_artefacts")
        self.view_all_artefacts = QtWidgets.QAction(main)
        self.view_all_artefacts.setObjectName("view_all_artefacts")
        self.project.addAction(self.quit)
        self.settings.addAction(self.scheduler)
        self.help.addAction(self.about)
        self.menuView.addAction(self.view_public_artefacts)
        self.menuView.addAction(self.view_all_artefacts)
        self.menubar.addAction(self.project.menuAction())
        self.menubar.addAction(self.menuView.menuAction())
        self.menubar.addAction(self.settings.menuAction())
        self.menubar.addAction(self.help.menuAction())

        self.retranslateUi(main)
        QtCore.QMetaObject.connectSlotsByName(main)

    def retranslateUi(self, main):
        _translate = QtCore.QCoreApplication.translate
        main.setWindowTitle(_translate("main", "Faber Bench"))
        self.sourcedir.setText(_translate("main", "TextLabel"))
        self.sourcedir_label.setText(_translate("main", "source directory:"))
        self.builddir_label.setText(_translate("main", "build directory:"))
        self.logo.setText(_translate("main", "<span>&nbsp;&nbsp;&nbsp;Faber</span>"))
        self.builddir.setText(_translate("main", "TextLabel"))
        self.project.setTitle(_translate("main", "Pro&ject"))
        self.settings.setTitle(_translate("main", "Setti&ngs"))
        self.help.setTitle(_translate("main", "He&lp"))
        self.menuView.setTitle(_translate("main", "View"))
        self.toolbar.setWindowTitle(_translate("main", "toolBar"))
        self.quit.setText(_translate("main", "&Quit"))
        self.scheduler.setText(_translate("main", "&Scheduler"))
        self.about.setText(_translate("main", "&About"))
        self.view_public_artefacts.setText(_translate("main", "&view public artefacts"))
        self.view_all_artefacts.setText(_translate("main", "v&iew all artefacts"))

from .widgets import ProgressBar
from . import resource_rc
